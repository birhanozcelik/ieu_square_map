cmake_minimum_required(VERSION 2.8.3)
project(ieu_square_map)

## Add support for C++11, supported in ROS Kinetic and newer
add_definitions(-std=c++11)

# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED COMPONENTS 
  roscpp
  rospy
  std_msgs
  message_generation
  gazebo_ros 
)


## Generate added messages and services
generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
  DEPENDS 
    roscpp 
    gazebo_ros
    message_runtime
)


# Depend on system install of Gazebo
find_package(gazebo REQUIRED)

link_directories(${GAZEBO_LIBRARY_DIRS})
include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS})

# For checkpoint plugin
catkin_package(
  DEPENDS 
    roscpp 
    gazebo_ros 
)
